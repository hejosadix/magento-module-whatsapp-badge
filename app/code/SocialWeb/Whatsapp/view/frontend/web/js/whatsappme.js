(function (jQuery_1_12_4) {
  'use strict';

  jQuery_1_12_4(function () {
    var delay_on_start = 3000;
    var $whatsappme = jQuery_1_12_4('.whatsappme');
    var $badge = $whatsappme.find('.whatsappme__badge');
    var wame_settings = $whatsappme.data('settings');
    var store;
    try {
      localStorage.setItem('test', 1);
      localStorage.removeItem('test');
      store = localStorage;
    } catch (e) {
      store = {
        _data: {},
        setItem: function (id, val) { this._data[id] = String(val); },
        getItem: function (id) { return this._data.hasOwnProperty(id) ? this._data[id] : null; }
      };
    }

    if (typeof (wame_settings) == 'undefined') {
      try {
        wame_settings = JSON.parse($whatsappme.attr('data-settings'));
      } catch (error) {
        wame_settings = undefined;
      }
    }
    if ($whatsappme.length && !!wame_settings && !!wame_settings.telephone) {
      whatsappme_magic();
    }

    function whatsappme_magic() {
      var is_mobile = !!navigator.userAgent.match(/Android|iPhone|BlackBerry|IEMobile|Opera Mini/i);
      var has_cta = wame_settings.message_text !== '';
      var message_hash, is_viewed, timeoutID;

      var messages_viewed = (store.getItem('whatsappme_hashes') || '').split(',').filter(Boolean);
      var is_second_visit = store.getItem('whatsappme_visited') == 'yes';

      if (has_cta) {
        message_hash = hash(wame_settings.message_text).toString();
        is_viewed = messages_viewed.indexOf(message_hash) > -1;
      }

      store.setItem('whatsappme_visited', 'yes');

      if (!wame_settings.mobile_only || is_mobile) {
    
        setTimeout(function () { $whatsappme.addClass('whatsappme--show'); }, delay_on_start);

        if (has_cta && !is_viewed) {
          if (wame_settings.message_badge) { 
            setTimeout(function () { $badge.addClass('whatsappme__badge--in'); }, delay_on_start + wame_settings.message_delay);
          } else if (is_second_visit) { 
            setTimeout(function () { $whatsappme.addClass('whatsappme--dialog'); }, delay_on_start + wame_settings.message_delay);
          }
        }
      }

      if (has_cta && !is_mobile) {
        jQuery_1_12_4('.whatsappme__button')
          .mouseenter(function () { timeoutID = setTimeout(show_dialog, 1500); })
          .mouseleave(function () { clearTimeout(timeoutID); });
      }

      jQuery_1_12_4('.whatsappme__button').click(function () {
      
        var link = whatsapp_link(wame_settings.telephone, wame_settings.message_send);

        if (has_cta && !$whatsappme.hasClass('whatsappme--dialog')) {
          show_dialog();
        } else {
          $whatsappme.removeClass('whatsappme--dialog');
          save_message_viewed();
          send_event(link);
          
          window.open(link, 'whatsappme');
        }
      });

      jQuery_1_12_4('.whatsappme__close').click(function () {
        $whatsappme.removeClass('whatsappme--dialog');
        save_message_viewed();
      });

      function show_dialog() {
        $whatsappme.addClass('whatsappme--dialog');

        if (wame_settings.message_badge && $badge.hasClass('whatsappme__badge--in')) {
          $badge.removeClass('whatsappme__badge--in').addClass('whatsappme__badge--out');
          save_message_viewed();
        }
      }

      function save_message_viewed() {
        if (has_cta && !is_viewed) {
          messages_viewed.push(message_hash)
          store.setItem('whatsappme_hashes', messages_viewed.join(','));
          is_viewed = true;
        }
      }
    }
  });

  function hash(s) {
    for (var i = 0, h = 1; i < s.length; i++) {
      h = Math.imul(h + s.charCodeAt(i) | 0, 2654435761);
    }
    return (h ^ h >>> 17) >>> 0;
  };

  function whatsapp_link(phone, message) {
    var link = 'https://api.whatsapp.com/send?phone=' + phone;
    if (typeof (message) == 'string' && message != '') {
      link += '&text=' + encodeURIComponent(message);
    }

    return link;
  }

  function send_event(link) {
    if (typeof gtag == 'function') { 
      gtag('event', 'click', {
        'event_category': 'WhatsAppMe',
        'event_label': link,
        'transport_type': 'beacon'
      });
    } else if (typeof ga == 'function') { 
      ga('send', 'event', {
        'eventCategory': 'WhatsAppMe',
        'eventAction': 'click',
        'eventLabel': link,
        'transport': 'beacon'
      });
    }
  }

  Math.imul = Math.imul || function (a, b) {
    var ah = (a >>> 16) & 0xffff;
    var al = a & 0xffff;
    var bh = (b >>> 16) & 0xffff;
    var bl = b & 0xffff;
    return ((al * bl) + (((ah * bl + al * bh) << 16) >>> 0) | 0);
  };

}(jQuery_1_12_4));
