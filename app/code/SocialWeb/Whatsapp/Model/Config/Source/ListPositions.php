<?php
namespace SocialWeb\Whatsapp\Model\Config\Source;

class ListPositions implements \Magento\Framework\Option\ArrayInterface
{
 public function toOptionArray()
 {
  return [
    ['value' => 'right', 'label' => __('right')],
    ['value' => 'left', 'label' => __('left')]
  ];
 }
}